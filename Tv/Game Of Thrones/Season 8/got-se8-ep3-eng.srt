1
00:01:33,698 --> 00:01:44,091
<font color="#ec14bd">Sync & corrections by honeybunny
www.NONTONKITA.com</font>

2
00:01:58,614 --> 00:01:59,698
Oi!

3
00:02:01,531 --> 00:02:02,573
Mind out.

4
00:02:04,865 --> 00:02:05,865
Move!

5
00:02:20,422 --> 00:02:22,886
Look, on the west wall!

6
00:02:23,158 --> 00:02:25,075
Get down to the crypt!

7
00:02:27,881 --> 00:02:29,088
Just hold.

8
00:02:30,172 --> 00:02:31,380
We'll take him.

9
00:02:35,839 --> 00:02:38,088
- Come on, help the lads!
- Yes, my lady.

10
00:02:38,172 --> 00:02:41,339
Quickly, now! Quickly!
Get those in position.

11
00:02:41,422 --> 00:02:44,505
- Come on.
- Close the gate!

12
00:02:44,589 --> 00:02:46,213
That's it.

13
00:05:25,422 --> 00:05:28,963
Oh, for fuck's sake.
You took your time.

14
00:07:32,589 --> 00:07:34,547
Do you speak their tongue?

15
00:07:38,130 --> 00:07:40,547
Tell them to lift their swords.

16
00:09:24,255 --> 00:09:25,881
Valar morghulis.

17
00:09:25,963 --> 00:09:27,881
Valar dohaeris.

18
00:09:42,672 --> 00:09:43,797
Open the gate!

19
00:09:43,881 --> 00:09:45,297
Open the gate!

20
00:09:45,380 --> 00:09:47,380
Open the gate!

21
00:10:02,380 --> 00:10:04,172
Come. Come on.

22
00:10:05,547 --> 00:10:06,547
Easy.

23
00:10:33,130 --> 00:10:35,922
There's no need to execute me,
Ser Davos.

24
00:10:36,005 --> 00:10:38,589
I'll be dead before the dawn.

25
00:12:38,339 --> 00:12:41,714
_

26
00:14:21,797 --> 00:14:23,672
The Night King is coming.

27
00:14:25,422 --> 00:14:27,422
The dead are already here.

28
00:15:52,088 --> 00:15:53,756
Stand your ground!

29
00:17:43,130 --> 00:17:44,881
Get down to the crypt.

30
00:17:46,172 --> 00:17:48,088
I'm not abandoning my people.

31
00:17:48,172 --> 00:17:49,672
Take this and go.

32
00:17:53,213 --> 00:17:54,839
I don't know how to use it.

33
00:17:56,047 --> 00:17:57,839
Stick them with the pointy end.

34
00:19:24,547 --> 00:19:25,922
Edd!

35
00:19:27,088 --> 00:19:28,380
Edd!

36
00:19:47,297 --> 00:19:48,589
Sam, get up!

37
00:19:53,547 --> 00:19:55,797
Oh, God. Sam.

38
00:19:59,963 --> 00:20:01,339
Edd!

39
00:21:42,922 --> 00:21:45,589
Fall back! Fall back!

40
00:21:47,881 --> 00:21:49,963
Fall back!

41
00:21:50,047 --> 00:21:51,130
Come on!

42
00:21:52,839 --> 00:21:53,797
Open the gate!

43
00:21:53,881 --> 00:21:55,797
Open the gate!

44
00:21:55,881 --> 00:21:57,088
Let us in!

45
00:21:59,297 --> 00:22:00,797
Open the gate!

46
00:22:00,881 --> 00:22:02,255
Open the gate!

47
00:22:05,088 --> 00:22:06,255
Keep moving!

48
00:22:10,047 --> 00:22:11,380
Come on!

49
00:22:14,589 --> 00:22:17,172
- Keep moving, fast.
- To your posts.

50
00:22:17,255 --> 00:22:19,255
- Go!
- Keep moving!

51
00:22:21,088 --> 00:22:22,839
Come on, run!

52
00:22:22,922 --> 00:22:24,088
Keep moving!

53
00:22:30,714 --> 00:22:32,969
_

54
00:22:36,085 --> 00:22:38,290
_

55
00:23:19,464 --> 00:23:20,839
Dany!

56
00:23:47,631 --> 00:23:49,547
Grab your weapon now!

57
00:23:49,631 --> 00:23:51,213
Guard the barricades!

58
00:23:51,297 --> 00:23:53,547
Through that pass!

59
00:23:53,631 --> 00:23:55,130
Hey! Hey! Hey!

60
00:23:55,213 --> 00:23:56,631
Close up the gaps!

61
00:23:56,714 --> 00:23:59,631
Go! Hey! Go! Go!

62
00:23:59,714 --> 00:24:01,631
Get in! Get in!

63
00:24:01,714 --> 00:24:03,380
Get in! Come on!

64
00:24:38,172 --> 00:24:39,797
Move!

65
00:25:09,589 --> 00:25:13,130
_

66
00:25:13,213 --> 00:25:14,714
_

67
00:25:14,797 --> 00:25:16,839
- Come on!
- Come on!

68
00:25:16,922 --> 00:25:18,464
- Come on!
- Come on!

69
00:25:19,922 --> 00:25:21,255
Protect the gate!

70
00:25:21,339 --> 00:25:23,380
Protect the gate!

71
00:25:23,464 --> 00:25:25,963
They're coming! Come on!

72
00:25:26,047 --> 00:25:27,631
Fall back!

73
00:25:47,547 --> 00:25:48,589
Light the trench!

74
00:25:49,380 --> 00:25:50,797
Light the trench!

75
00:25:52,088 --> 00:25:54,505
- Wait!
- Light the trench!

76
00:26:23,839 --> 00:26:25,047
She can't see us.

77
00:26:26,797 --> 00:26:27,839
Light the trenches!

78
00:26:42,339 --> 00:26:44,464
With a torch, light the trenches!

79
00:26:44,547 --> 00:26:47,005
- Right there.
- Torch it!

80
00:26:48,631 --> 00:26:49,714
Light the trenches!

81
00:29:34,589 --> 00:29:35,547
Clegane.

82
00:29:41,714 --> 00:29:44,714
You must try not to worry yourself.

83
00:29:56,714 --> 00:29:59,881
At least we're already in a crypt.

84
00:30:04,589 --> 00:30:06,547
If we were up there,

85
00:30:06,631 --> 00:30:10,130
we might see something
everyone else is missing.

86
00:30:11,589 --> 00:30:13,213
Something that makes a difference.

87
00:30:13,297 --> 00:30:16,005
What?

88
00:30:16,088 --> 00:30:17,714
Remember the Battle of Blackwater?

89
00:30:17,797 --> 00:30:19,130
I brought us through the Mud Gate.

90
00:30:19,213 --> 00:30:20,631
And got your face cut in half.

91
00:30:20,714 --> 00:30:22,422
And it made a difference.

92
00:30:24,922 --> 00:30:26,505
If I was out there right now...

93
00:30:26,589 --> 00:30:27,963
You'd die.

94
00:30:30,297 --> 00:30:32,297
There's nothing you can do.

95
00:30:35,881 --> 00:30:38,172
You might be surprised
at the lengths I'd go

96
00:30:38,255 --> 00:30:40,672
to avoid joining the Army of the Dead.

97
00:30:40,756 --> 00:30:43,963
I could think of no organization
less suited to my talents.

98
00:30:44,047 --> 00:30:46,464
Witty remarks won't make a difference.

99
00:30:48,839 --> 00:30:52,005
That's why we're down here,
none of us can do anything.

100
00:30:53,839 --> 00:30:54,922
It's the truth.

101
00:30:56,172 --> 00:30:58,963
It's the most heroic thing
we can do now...

102
00:31:00,422 --> 00:31:02,464
look the truth in the face.

103
00:31:07,297 --> 00:31:09,714
Maybe we should have stayed married.

104
00:31:09,797 --> 00:31:11,631
You were the best of them.

105
00:31:12,839 --> 00:31:15,005
What a terrifying thought.

106
00:31:21,963 --> 00:31:23,464
It wouldn't work between us.

107
00:31:24,797 --> 00:31:26,464
Why not?

108
00:31:26,547 --> 00:31:27,963
The Dragon Queen.

109
00:31:29,963 --> 00:31:33,339
Your divided loyalties
would become a problem.

110
00:31:34,339 --> 00:31:36,088
Yes.

111
00:31:36,172 --> 00:31:39,005
Without the Dragon Queen,
there'd be no problem at all.

112
00:31:40,130 --> 00:31:41,714
We'd all be dead already.

113
00:32:03,464 --> 00:32:05,339
They lit the trench.

114
00:32:19,047 --> 00:32:20,047
Bran...

115
00:32:25,464 --> 00:32:27,380
I just want you to know...

116
00:32:31,547 --> 00:32:32,505
I wish...

117
00:32:33,756 --> 00:32:35,088
The things I did...

118
00:32:35,172 --> 00:32:38,088
Everything you did
brought you where you are now.

119
00:32:40,380 --> 00:32:41,756
Where you belong.

120
00:32:43,881 --> 00:32:45,047
Home.

121
00:32:55,756 --> 00:32:57,922
I'm going to go now.

122
00:33:00,589 --> 00:33:01,589
Go where?

123
00:34:50,339 --> 00:34:51,672
Man the walls!

124
00:34:52,714 --> 00:34:54,339
Man the walls!

125
00:35:02,547 --> 00:35:04,714
Man the wall!

126
00:35:04,797 --> 00:35:06,380
Man the walls!

127
00:35:08,213 --> 00:35:09,963
Come on!

128
00:35:10,047 --> 00:35:12,839
Get out there! Get moving!

129
00:35:12,922 --> 00:35:15,047
Come on. Come on! Come on!

130
00:35:18,505 --> 00:35:20,881
Go, go, go!

131
00:35:20,963 --> 00:35:22,422
Come on!

132
00:36:01,922 --> 00:36:03,797
They're against the wall!

133
00:36:07,922 --> 00:36:10,130
Get more men up here!

134
00:36:14,297 --> 00:36:16,380
- Fill the gaps, quickly!
- Get in there!

135
00:36:16,464 --> 00:36:18,005
They're climbing the walls!

136
00:36:18,088 --> 00:36:20,631
- Relieve the archers!
- Relieve the archers!

137
00:36:22,505 --> 00:36:23,881
Archers on top!

138
00:36:23,963 --> 00:36:26,714
- Move up!
- Go, go! Come here!

139
00:36:26,797 --> 00:36:28,339
- Come on!
- Move back!

140
00:36:28,422 --> 00:36:30,339
- Stand away!
- Go on, go on. Go! Go.

141
00:36:30,422 --> 00:36:32,756
- Step back.
- All the way, archers!

142
00:36:48,464 --> 00:36:49,714
Hold the wall!

143
00:36:49,797 --> 00:36:51,339
The wall, they're coming up!

144
00:36:51,422 --> 00:36:54,881
- Hold the wall!
- Hold the wall!

145
00:36:54,963 --> 00:36:56,130
Draw!

146
00:37:20,839 --> 00:37:22,005
Come on!

147
00:38:12,881 --> 00:38:14,213
They're coming up!

148
00:38:26,422 --> 00:38:27,589
Look out!

149
00:40:06,589 --> 00:40:07,714
Clegane!

150
00:40:09,172 --> 00:40:10,297
Clegane!

151
00:41:20,797 --> 00:41:22,714
Clegane!

152
00:41:22,797 --> 00:41:25,005
Clegane, we need you!

153
00:41:25,088 --> 00:41:26,339
You can't give up on us.

154
00:41:26,422 --> 00:41:28,631
Fuck off! We can't beat them.

155
00:41:28,714 --> 00:41:31,213
Don't you see that, you stupid whore?

156
00:41:31,297 --> 00:41:33,005
We're fighting Death!

157
00:41:33,088 --> 00:41:34,422
They can't beat Death.

158
00:41:39,339 --> 00:41:40,589
Tell her that.

159
00:49:15,088 --> 00:49:16,380
Open the door!

160
00:49:19,005 --> 00:49:20,339
Open the door!

161
00:49:20,422 --> 00:49:22,172
- Come on!
- Open the door!

162
00:49:22,255 --> 00:49:23,672
Open the door!

163
00:49:28,464 --> 00:49:30,297
Please!

164
00:49:30,380 --> 00:49:31,547
Open it!

165
00:50:48,631 --> 00:50:50,297
Rah!

166
00:50:57,047 --> 00:50:58,130
Aah!

167
00:51:06,339 --> 00:51:08,922
Come on! Go!

168
00:51:09,005 --> 00:51:11,881
- Come on!
- Run!

169
00:51:38,088 --> 00:51:39,380
We've gotta go!

170
00:52:40,797 --> 00:52:43,881
The Lord brought him back for a purpose.

171
00:52:48,255 --> 00:52:50,464
Now that purpose has been served.

172
00:52:55,922 --> 00:52:56,922
I know you.

173
00:53:02,631 --> 00:53:03,797
And I know you.

174
00:53:11,589 --> 00:53:13,881
You said we'd meet again.

175
00:53:13,963 --> 00:53:15,172
And here we are.

176
00:53:16,213 --> 00:53:17,505
At the end of the world.

177
00:53:19,631 --> 00:53:21,589
You said I'd shut many eyes forever.

178
00:53:23,255 --> 00:53:24,797
You were right about that too.

179
00:53:25,881 --> 00:53:27,714
Brown eyes...

180
00:53:27,797 --> 00:53:29,005
green eyes...

181
00:53:31,339 --> 00:53:32,672
and blue eyes.

182
00:53:52,130 --> 00:53:54,839
What do we say to the God of Death?

183
00:53:59,088 --> 00:54:00,172
Not today.

184
00:54:25,672 --> 00:54:26,714
Here they come!

185
00:54:30,464 --> 00:54:31,547
Steady, lads.

186
00:54:32,922 --> 00:54:34,005
Steady now.

187
00:54:36,839 --> 00:54:38,213
Make every shot count.

188
00:54:47,631 --> 00:54:48,714
Over there!

189
00:54:51,963 --> 00:54:53,005
There!

190
00:55:33,464 --> 00:55:34,756
Get off.

191
00:57:11,963 --> 00:57:13,047
Dracarys.

192
01:01:56,922 --> 01:01:58,464
Come on! Come on!

193
01:02:00,631 --> 01:02:02,963
No! No, no, no!

194
01:02:52,839 --> 01:02:55,213
- Bran!
- Go!

195
01:08:13,172 --> 01:08:14,213
Jorah!

196
01:10:59,797 --> 01:11:00,963
Theon.

197
01:11:12,797 --> 01:11:14,339
You're a good man.

198
01:11:20,380 --> 01:11:21,380
Thank you.

199
01:16:45,213 --> 01:16:46,881
I'm hurt.

200
01:19:52,464 --> 01:20:02,548
<font color="#ec14bd">Sync & corrections by honeybunny
www.NONTONKITA.com</font>

201
01:21:25,255 --> 01:21:27,339
We have won the Great War.

202
01:21:29,172 --> 01:21:30,756
Now, we will win the last war.

203
01:21:38,756 --> 01:21:40,839
We'll rip her out
root and stem.

