1
00:00:04,224 --> 00:00:05,974
We have won the Great War.

2
00:00:07,099 --> 00:00:08,557
Now we will win the last war.

3
00:00:08,933 --> 00:00:11,116
The objective
here is to remove Cersei

4
00:00:11,140 --> 00:00:13,015
without destroying King's Landing.

5
00:00:15,349 --> 00:00:16,989
You heading to King's Landing?

6
00:00:17,182 --> 00:00:19,015
There's some
unfinished business.

7
00:00:19,307 --> 00:00:20,307
Me too.

8
00:00:20,474 --> 00:00:22,307
I don't plan on coming back.

9
00:00:23,140 --> 00:00:24,474
Neither do I.

10
00:00:26,099 --> 00:00:29,349
I always wanted to be there
when they execute your sister.

11
00:00:30,307 --> 00:00:31,974
Seems like I won't get the chance.

12
00:00:33,224 --> 00:00:34,904
Stay with me.

13
00:00:35,974 --> 00:00:37,140
Please.

14
00:00:37,557 --> 00:00:38,974
Cersei's hateful...

15
00:00:41,474 --> 00:00:42,516
And so am I.

16
00:00:51,391 --> 00:00:52,849
I need to tell you something.

17
00:00:55,349 --> 00:00:58,307
But you have to swear
you'll never tell another soul.

18
00:00:58,391 --> 00:01:01,474
What if there's someone else?
Someone better.

19
00:01:01,933 --> 00:01:04,683
At a certain point, you choose
a person you believe in,

20
00:01:04,766 --> 00:01:06,099
and you fight for that person.

21
00:01:06,182 --> 00:01:08,391
Even if you know it's a mistake?

22
00:01:11,182 --> 00:01:12,702
He has the better claim to the throne.

23
00:01:12,766 --> 00:01:14,367
Every time
a Targaryen is born,

24
00:01:14,391 --> 00:01:15,391
the gods flip a coin.

25
00:01:15,808 --> 00:01:17,450
The Mad King
gave his enemies the justice

26
00:01:17,474 --> 00:01:18,516
he thought they deserved.

27
00:01:18,599 --> 00:01:20,119
Children are not their fathers.

28
00:01:20,391 --> 00:01:21,991
- Be a dragon.
- You have a gentle heart.

29
00:01:22,015 --> 00:01:24,432
A Targaryen
alone in the world

30
00:01:24,516 --> 00:01:25,724
is a terrible thing.

31
00:01:26,057 --> 00:01:28,116
You don't
want to awake the dragon, do you?

32
00:03:12,664 --> 00:03:20,664
<font color="#ec14bd">Sync & corrections by honeybunny
www.NONTONKITA.com</font>

33
00:03:57,349 --> 00:03:58,349
Come in.

34
00:04:02,265 --> 00:04:03,391
And?

35
00:04:05,182 --> 00:04:06,724
Nothing?

36
00:04:06,808 --> 00:04:08,432
She won't eat.

37
00:04:13,307 --> 00:04:15,516
We'll try again at supper.

38
00:04:17,766 --> 00:04:19,432
I think they're watching me.

39
00:04:20,432 --> 00:04:24,349
- Who?
- Her soldiers.

40
00:04:24,432 --> 00:04:28,391
Of course they are.
That's their job.

41
00:04:38,557 --> 00:04:40,349
What have I told you, Martha?

42
00:04:41,683 --> 00:04:44,891
The greater the risk,
the greater the reward.

43
00:04:46,224 --> 00:04:49,099
Go on. They'll be missing you
in the kitchen.

44
00:05:12,641 --> 00:05:14,265
The Northern armies?

45
00:05:14,349 --> 00:05:15,849
Just crossed the Trident.

46
00:05:15,933 --> 00:05:18,808
They'll be at the walls
of King's Landing in two days.

47
00:05:19,974 --> 00:05:21,474
How is she?

48
00:05:21,557 --> 00:05:24,182
She hasn't seen anyone
since we returned.

49
00:05:24,265 --> 00:05:27,599
Hasn't left her chambers,
hasn't accepted any food.

50
00:05:28,766 --> 00:05:30,516
She shouldn't be alone.

51
00:05:30,599 --> 00:05:33,683
You're worried for her.
I admire your empathy.

52
00:05:33,766 --> 00:05:35,641
Aren't you worried for her?

53
00:05:35,724 --> 00:05:37,849
I'm worried for all of us.

54
00:05:37,933 --> 00:05:40,099
They say every time
a Targaryen is born,

55
00:05:40,182 --> 00:05:43,391
the gods toss a coin
and the world holds its breath.

56
00:05:43,474 --> 00:05:46,224
We're not much for riddles
where I'm from.

57
00:05:46,307 --> 00:05:49,140
We both know what she's about to do.

58
00:05:55,015 --> 00:05:57,140
That's her decision to make.
She is our queen.

59
00:05:57,224 --> 00:06:01,349
Men decide where power resides,
whether or not they know it.

60
00:06:03,891 --> 00:06:05,683
What do you want?

61
00:06:05,766 --> 00:06:07,516
All I've ever wanted.

62
00:06:07,599 --> 00:06:10,516
The right ruler on the Iron Throne.

63
00:06:12,057 --> 00:06:14,891
I still don't know
how her coin has landed.

64
00:06:14,974 --> 00:06:18,307
But I'm quite certain about yours.

65
00:06:26,349 --> 00:06:28,265
I don't want it.

66
00:06:28,349 --> 00:06:29,683
I never have.

67
00:06:32,140 --> 00:06:35,849
I have known more kings
and queens than any man living.

68
00:06:35,933 --> 00:06:37,599
I've heard what they say to crowds,

69
00:06:37,683 --> 00:06:39,516
and seen what they do in the shadows.

70
00:06:39,599 --> 00:06:42,974
I have furthered their designs,
however horrible.

71
00:06:43,057 --> 00:06:45,766
But what I tell you now is true:

72
00:06:45,849 --> 00:06:49,057
You will rule wisely and well,
while she...

73
00:06:49,140 --> 00:06:50,933
She...

74
00:06:51,015 --> 00:06:52,099
is my queen.

75
00:07:13,933 --> 00:07:15,391
Your Grace?

76
00:07:40,599 --> 00:07:43,057
There's something you need to know.

77
00:07:44,349 --> 00:07:46,599
Someone has betrayed me.

78
00:07:52,766 --> 00:07:54,140
Yes.

79
00:07:58,099 --> 00:07:59,891
Jon Snow.

80
00:08:05,766 --> 00:08:06,766
Varys.

81
00:08:08,933 --> 00:08:11,057
He knows the truth about Jon.

82
00:08:12,349 --> 00:08:13,891
He does.

83
00:08:18,641 --> 00:08:20,974
Because you told him.

84
00:08:23,265 --> 00:08:25,015
You learned from Sansa.

85
00:08:25,974 --> 00:08:27,641
And she learned from Jon,

86
00:08:27,724 --> 00:08:30,307
though I begged him not to tell her.

87
00:08:31,683 --> 00:08:33,516
As I said...

88
00:08:34,265 --> 00:08:35,349
he betrayed me.

89
00:08:35,432 --> 00:08:37,265
I'm glad Sansa told me.

90
00:08:37,349 --> 00:08:38,891
I am your Hand.

91
00:08:38,974 --> 00:08:41,474
I need to be aware
of any threats you're facing.

92
00:08:42,974 --> 00:08:44,265
And Varys?

93
00:08:44,349 --> 00:08:47,474
Your Master of Whisperers
needs to be aware too.

94
00:08:48,307 --> 00:08:50,724
You spoke to him first.

95
00:08:50,808 --> 00:08:52,057
Without coming to me.

96
00:08:52,140 --> 00:08:54,599
Without asking my permission.

97
00:08:56,224 --> 00:08:58,849
It was a mistake.

98
00:09:00,849 --> 00:09:03,641
Why do you think Sansa told you?

99
00:09:03,724 --> 00:09:05,432
What do you think she hoped to gain?

100
00:09:05,516 --> 00:09:09,140
- She trusts me.
- Yes, she trusts you.

101
00:09:10,265 --> 00:09:14,349
She trusted you to spread secrets

102
00:09:14,432 --> 00:09:17,516
that could destroy your own queen.

103
00:09:19,516 --> 00:09:21,808
And you did not let her down.

104
00:09:25,474 --> 00:09:29,015
If I have failed you, my queen,
forgive me.

105
00:09:30,891 --> 00:09:33,182
Our intentions were good.

106
00:09:34,599 --> 00:09:38,182
We wanted what you want.
A better world, all of us.

107
00:09:39,432 --> 00:09:42,265
Varys as much as anyone.

108
00:09:50,974 --> 00:09:53,099
But it doesn't matter now.

109
00:09:55,265 --> 00:09:56,265
No.

110
00:09:57,557 --> 00:09:59,224
It doesn't matter now.

111
00:11:59,683 --> 00:12:01,641
It was me.

112
00:12:07,015 --> 00:12:08,641
I hope I deserve this.

113
00:12:08,724 --> 00:12:10,391
Truly, I do.

114
00:12:10,474 --> 00:12:12,557
I hope I'm wrong.

115
00:12:20,099 --> 00:12:21,182
Goodbye, old friend.

116
00:13:02,683 --> 00:13:03,766
Lord Varys.

117
00:13:05,516 --> 00:13:07,683
I, Daenerys of House Targaryen,

118
00:13:07,766 --> 00:13:09,599
First of My Name,

119
00:13:09,683 --> 00:13:12,933
Breaker of Chains
and Mother of Dragons...

120
00:13:14,766 --> 00:13:16,808
sentence you to die.

121
00:13:27,307 --> 00:13:28,432
Dracarys.

122
00:14:19,599 --> 00:14:21,182
This was all she brought with her

123
00:14:21,265 --> 00:14:23,182
when we crossed the Narrow Sea.

124
00:14:24,265 --> 00:14:26,307
Her only possession.

125
00:15:11,015 --> 00:15:13,224
_

126
00:15:13,344 --> 00:15:14,706
_

127
00:15:42,015 --> 00:15:44,641
What did I say would
happen if you told your sister?

128
00:15:44,724 --> 00:15:48,015
I don't want it,
and that's what I told him.

129
00:15:49,307 --> 00:15:51,516
She betrayed your trust.

130
00:15:51,599 --> 00:15:53,349
She killed Varys as much as I did.

131
00:15:54,974 --> 00:15:57,432
This was a victory for her.

132
00:15:58,933 --> 00:16:00,140
Now she knows what happens

133
00:16:00,224 --> 00:16:02,140
when people hear the truth about you.

134
00:16:04,766 --> 00:16:08,766
Far more people in Westeros
love you than love me.

135
00:16:11,265 --> 00:16:13,641
I don't have love here.

136
00:16:15,099 --> 00:16:17,140
I only have fear.

137
00:16:20,057 --> 00:16:21,933
I love you.

138
00:16:24,474 --> 00:16:27,265
And you will always be my queen.

139
00:16:34,099 --> 00:16:36,099
Is that all I am to you?

140
00:16:37,849 --> 00:16:39,808
Your queen?

141
00:17:16,683 --> 00:17:18,641
All right, then.

142
00:17:26,015 --> 00:17:27,849
Let it be fear.

143
00:17:31,849 --> 00:17:35,099
The people who live
there, they're not your enemies.

144
00:17:35,182 --> 00:17:37,933
They're innocents, like the ones
you liberated in Meereen.

145
00:17:38,015 --> 00:17:39,991
In Meereen,
the slaves turned on the masters

146
00:17:40,015 --> 00:17:43,099
and liberated the city
themselves the moment I arrived.

147
00:17:43,182 --> 00:17:44,391
They're afraid.

148
00:17:44,474 --> 00:17:45,724
Anyone who resists Cersei

149
00:17:45,808 --> 00:17:47,933
will see his family butchered.

150
00:17:48,015 --> 00:17:50,224
You can't expect them to be heroes.

151
00:17:50,307 --> 00:17:52,432
- They're hostages.
- They are.

152
00:17:52,516 --> 00:17:55,849
In a tyrant's grip.
Whose fault is that? Mine?

153
00:17:55,933 --> 00:17:58,557
What does it matter whose fault it is?

154
00:17:58,641 --> 00:18:01,516
Thousands of children will die
if the city burns.

155
00:18:01,599 --> 00:18:02,849
Your sister knows how to use

156
00:18:02,933 --> 00:18:04,974
her enemies' weaknesses against them.

157
00:18:05,057 --> 00:18:07,474
That's what she thinks our mercy is:

158
00:18:07,557 --> 00:18:08,724
weakness.

159
00:18:08,808 --> 00:18:10,891
- I beg you, my queen...
- But she's wrong.

160
00:18:11,766 --> 00:18:13,557
Mercy is our strength.

161
00:18:14,808 --> 00:18:17,182
Our mercy towards future generations

162
00:18:17,265 --> 00:18:19,849
who will never again
be held hostage by a tyrant.

163
00:18:25,808 --> 00:18:27,432
Ready the Unsullied.

164
00:18:27,516 --> 00:18:29,057
Tonight you sail for King's Landing

165
00:18:29,140 --> 00:18:31,033
- to join the Northern armies.
- Cersei's followers

166
00:18:31,057 --> 00:18:32,933
will abandon her
if they know the war is lost.

167
00:18:33,015 --> 00:18:34,182
Give them that chance.

168
00:18:34,265 --> 00:18:36,557
If the city surrenders,

169
00:18:36,641 --> 00:18:38,683
they will ring the bells
and raise the gates.

170
00:18:38,766 --> 00:18:42,432
Please, if you hear them ringing
the bells, call off the attack.

171
00:18:50,599 --> 00:18:52,599
Wait for me outside the city.

172
00:18:53,683 --> 00:18:55,391
You'll know when it's time.

173
00:19:24,933 --> 00:19:28,808
Your brother was stopped
trying to get past our lines.

174
00:19:33,140 --> 00:19:36,641
It seems he hasn't abandoned
your sister after all.

175
00:19:42,349 --> 00:19:43,974
The next time you fail me...

176
00:19:45,349 --> 00:19:47,808
will be the last time you fail me.

177
00:20:04,599 --> 00:20:06,599
Let them pass.

178
00:20:18,015 --> 00:20:19,599
Through the gate.

179
00:20:19,683 --> 00:20:21,015
Get going.

180
00:21:02,599 --> 00:21:05,516
The rearguard
should be here by daybreak.

181
00:21:05,599 --> 00:21:08,057
She wants to attack now.

182
00:21:09,265 --> 00:21:10,724
Daybreak at the earliest.

183
00:21:12,224 --> 00:21:15,391
- Careful with that.
- My lord.

184
00:21:17,849 --> 00:21:19,057
Davos.

185
00:21:21,641 --> 00:21:23,933
I need to ask you a favor.

186
00:21:26,057 --> 00:21:28,557
You're the greatest smuggler
alive, aren't you?

187
00:21:30,099 --> 00:21:33,474
I'm not gonna like this favor, am I?

188
00:21:33,557 --> 00:21:36,015
He's always better
when he's got some food in him.

189
00:21:36,099 --> 00:21:39,265
Problem is when
he's got drink in him.

190
00:21:39,349 --> 00:21:40,808
See what he did the other week?

191
00:21:40,891 --> 00:21:42,474
Aye. Fighting as well.

192
00:21:42,557 --> 00:21:43,933
Commander nearly caught him.

193
00:21:44,015 --> 00:21:46,891
Ay up. Where you going?

194
00:21:48,307 --> 00:21:50,307
I'm Arya Stark.

195
00:21:50,391 --> 00:21:52,933
I'm going to kill Queen Cersei.

196
00:21:57,432 --> 00:21:58,432
Think about it.

197
00:21:58,516 --> 00:22:01,140
She kills Cersei, the war's over.

198
00:22:01,224 --> 00:22:04,599
There won't be a siege.
You might not even die tomorrow.

199
00:22:07,891 --> 00:22:09,724
I need to go talk to my captain.

200
00:22:09,808 --> 00:22:11,808
Go ahead, talk to him.

201
00:22:14,557 --> 00:22:16,265
Where's he going?

202
00:22:30,307 --> 00:22:33,808
_

203
00:22:35,488 --> 00:22:38,379
_

204
00:22:42,925 --> 00:22:46,045
_

205
00:22:46,391 --> 00:22:47,974
We speak the common tongue.

206
00:22:48,974 --> 00:22:50,891
Ah. Good.

207
00:22:50,974 --> 00:22:52,724
I want to be alone with the prisoner.

208
00:22:52,808 --> 00:22:54,974
Get some rest.
Tomorrow will be a long day.

209
00:22:55,057 --> 00:22:57,432
We have orders
to guard the prisoner.

210
00:22:57,516 --> 00:22:59,307
Ordered by whom?
The queen herself?

211
00:23:00,516 --> 00:23:02,474
- No.
- Well, in that case,

212
00:23:02,557 --> 00:23:05,516
as Hand of the Queen, I outrank
whomever gave your order.

213
00:23:05,599 --> 00:23:07,140
Probably by quite a lot.

214
00:23:30,849 --> 00:23:32,849
How did they find you?

215
00:23:41,015 --> 00:23:43,432
Did you consider taking it off?

216
00:23:44,891 --> 00:23:48,349
Cersei once called me
"the stupidest Lannister."

217
00:23:49,849 --> 00:23:53,099
And you're going back to her,
to die with her.

218
00:23:54,265 --> 00:23:55,891
You've underestimated her before.

219
00:23:55,974 --> 00:23:57,349
She's going to die.

220
00:23:58,516 --> 00:23:59,933
Unless you can convince her

221
00:24:00,015 --> 00:24:01,641
to change her course of action.

222
00:24:01,724 --> 00:24:03,724
Difficult to do from here.

223
00:24:09,349 --> 00:24:12,474
When have I ever been able
to convince Cersei of anything?

224
00:24:12,557 --> 00:24:14,140
Try.

225
00:24:14,224 --> 00:24:16,182
If not for yourself, if not for her,

226
00:24:16,265 --> 00:24:19,015
then for every one of the
million people in that city,

227
00:24:19,099 --> 00:24:20,432
innocent or otherwise.

228
00:24:20,516 --> 00:24:23,516
To be honest, I never really
cared much for them.

229
00:24:23,599 --> 00:24:25,265
Innocent or otherwise.

230
00:24:26,599 --> 00:24:28,724
You do care for one innocent.

231
00:24:29,641 --> 00:24:31,140
I know you do.

232
00:24:31,224 --> 00:24:32,974
And so does Cersei.

233
00:24:34,265 --> 00:24:35,974
She has a reason now.

234
00:24:37,432 --> 00:24:41,182
The child is the reason
she'll never give an inch.

235
00:24:41,265 --> 00:24:43,057
All the worst things she's ever done,

236
00:24:43,140 --> 00:24:45,516
she's done for her children.

237
00:24:45,599 --> 00:24:47,766
It's not impossible that she'll win.

238
00:24:47,849 --> 00:24:49,057
She won't.

239
00:24:49,140 --> 00:24:51,015
Her enemy's forces have been depleted,

240
00:24:51,099 --> 00:24:52,474
as she said they would be.

241
00:24:52,557 --> 00:24:54,808
Two of the three dragons are dead.

242
00:24:54,891 --> 00:24:57,432
- She's evened the odds.
- The city will fall tomorrow.

243
00:24:57,516 --> 00:24:59,492
She has the Lannister army,
she has the Golden Company...

244
00:24:59,516 --> 00:25:01,283
I defended the city
last time it was attacked.

245
00:25:01,307 --> 00:25:03,891
I know it better than anyone.
It will fall tomorrow.

246
00:25:03,974 --> 00:25:06,224
Then I suppose I'll die
tomorrow, if not before.

247
00:25:06,307 --> 00:25:07,974
Why?

248
00:25:14,182 --> 00:25:15,683
Escape.

249
00:25:15,766 --> 00:25:17,766
The two of you, together.

250
00:25:17,849 --> 00:25:19,099
Remember where we met,

251
00:25:19,182 --> 00:25:20,849
where they keep the dragon skulls,

252
00:25:20,933 --> 00:25:21,974
beneath the Red Keep?

253
00:25:23,641 --> 00:25:24,933
Take her down there.

254
00:25:25,015 --> 00:25:26,683
Keep following the stairways down,

255
00:25:26,766 --> 00:25:28,140
down as far as they'll go.

256
00:25:28,224 --> 00:25:30,557
You'll come out onto a beach
at the foot of the keep.

257
00:25:30,641 --> 00:25:32,599
A dinghy will be waiting for you.

258
00:25:33,808 --> 00:25:35,182
Sail out of the bay.

259
00:25:35,265 --> 00:25:37,974
If the winds are kind,
you'll make it to Pentos.

260
00:25:39,140 --> 00:25:41,557
Start a new life.

261
00:25:44,891 --> 00:25:48,224
Sail right past the Iron Fleet
and into a new life?

262
00:25:48,307 --> 00:25:50,974
Sounds a lot less likely
than Cersei winning this war...

263
00:25:51,057 --> 00:25:53,683
There won't be an Iron Fleet
for much longer.

264
00:25:54,891 --> 00:25:56,140
Do it.

265
00:25:56,224 --> 00:25:59,349
If you don't,
you'll never see Cersei again.

266
00:26:05,224 --> 00:26:07,015
Swear to me.

267
00:26:10,307 --> 00:26:12,224
You have my word.

268
00:26:16,849 --> 00:26:18,265
If it works,

269
00:26:18,349 --> 00:26:20,015
give the order to ring all the bells

270
00:26:20,099 --> 00:26:21,849
in King's Landing and open the gates.

271
00:26:21,933 --> 00:26:24,724
That will be our signal
that the city has surrendered.

272
00:26:24,808 --> 00:26:26,265
I'll try.

273
00:26:27,683 --> 00:26:30,057
I never thought
I'd get to repay the favor.

274
00:26:30,140 --> 00:26:32,849
Remember, ring the bells
and open the gates.

275
00:26:32,933 --> 00:26:35,474
Your queen will execute you for this.

276
00:26:35,557 --> 00:26:37,307
If Daenerys can make it to the throne

277
00:26:37,391 --> 00:26:39,391
without wading through
a river of blood,

278
00:26:39,474 --> 00:26:40,599
maybe she'll show mercy

279
00:26:40,683 --> 00:26:42,891
to the person who made that possible.

280
00:26:48,641 --> 00:26:51,391
Tens of thousands of innocent lives...

281
00:26:52,683 --> 00:26:55,891
one not particularly innocent dwarf...

282
00:26:57,307 --> 00:26:59,891
it seems like a fair trade.

283
00:27:06,432 --> 00:27:08,432
If it weren't for you,

284
00:27:08,516 --> 00:27:10,683
I never would've survived my childhood.

285
00:27:12,808 --> 00:27:14,641
You would have.

286
00:27:22,265 --> 00:27:23,808
You were the only one

287
00:27:25,015 --> 00:27:27,933
who didn't treat me like a monster.

288
00:27:30,516 --> 00:27:32,766
You were all I had.

289
00:28:43,641 --> 00:28:46,057
Eyes on the horizon!

290
00:28:47,349 --> 00:28:49,349
Load the scorpion!

291
00:29:02,599 --> 00:29:04,015
Archers, close up!

292
00:29:04,099 --> 00:29:05,474
Come on!

293
00:29:05,557 --> 00:29:07,557
Up to the espringal!

294
00:29:07,641 --> 00:29:09,849
To the back of it!

295
00:29:21,808 --> 00:29:23,516
Archers, line up!

296
00:29:31,557 --> 00:29:33,057
This way! Come on!

297
00:29:33,140 --> 00:29:34,724
Clear the streets now!

298
00:29:34,808 --> 00:29:37,057
- No!
- In, now.

299
00:29:37,140 --> 00:29:39,808
- And you, come on.
- Inside, now!

300
00:29:39,891 --> 00:29:41,307
Quickly.

301
00:29:41,391 --> 00:29:42,474
Yes. Follow down there.

302
00:29:42,557 --> 00:29:44,349
They're here.
Get to the Red Keep.

303
00:29:46,057 --> 00:29:48,015
- My son!
- Get inside!

304
00:29:52,349 --> 00:29:54,265
Just ahead of you, I can see it!

305
00:29:54,349 --> 00:29:55,683
All right, move it!

306
00:29:55,766 --> 00:29:57,599
- Come on!
- Keep moving!

307
00:29:57,683 --> 00:29:59,408
- Keep going!
- Keep moving!

308
00:29:59,432 --> 00:30:01,182
- Quickly!
- All the way!

309
00:30:01,265 --> 00:30:03,182
- Come on!
- Come on!

310
00:30:03,265 --> 00:30:05,391
Hold your mother's hand!

311
00:30:18,140 --> 00:30:22,057
Make way for the Golden Company!

312
00:30:31,099 --> 00:30:32,349
Close the gate!

313
00:31:36,808 --> 00:31:39,849
If you hear the bells ring,
they've surrendered.

314
00:31:39,933 --> 00:31:41,849
Call off your men.

315
00:31:54,182 --> 00:31:56,432
Move along now, lads.

316
00:32:25,182 --> 00:32:27,766
- That's it.
- Go on, move it.

317
00:32:31,015 --> 00:32:34,265
Come on, move, move.
Faster, faster. Come on, move it.

318
00:32:34,349 --> 00:32:36,182
- Keep going.
- Keep moving.

319
00:32:40,015 --> 00:32:42,391
- Move.
- Stay with him.

320
00:32:43,599 --> 00:32:45,182
- Hold on.
- Hold my hand.

321
00:32:45,265 --> 00:32:46,474
Get behind there.

322
00:32:46,557 --> 00:32:48,683
Close the gate!

323
00:32:48,766 --> 00:32:50,391
No more through.

324
00:32:53,265 --> 00:32:55,808
- No.
- No. Mommy.

325
00:32:55,891 --> 00:32:58,683
- Vicky! Please!
- No, please, sir!

326
00:32:58,766 --> 00:33:01,140
- Open the gate!
- Close the gate!

327
00:33:01,224 --> 00:33:03,057
- Open it!
- Open the gate!

328
00:33:03,140 --> 00:33:06,933
- Get back.
- Look, try that one. Go. Go.

329
00:33:12,766 --> 00:33:14,849
The gates are locked now!

330
00:33:14,933 --> 00:33:16,265
Open!

331
00:33:16,349 --> 00:33:18,516
- Get back!
- Open the gate!

332
00:33:18,599 --> 00:33:21,307
Soldier! Soldier!

333
00:33:21,391 --> 00:33:24,391
- Please open the gate!
- Go back to your homes!

334
00:33:24,474 --> 00:33:25,974
Stay back! It's closed!

335
00:33:26,057 --> 00:33:27,474
Soldier!

336
00:33:28,974 --> 00:33:30,265
Move away! Stop!

337
00:33:30,349 --> 00:33:33,015
Let us in!

338
00:33:35,474 --> 00:33:37,641
We're not leaving!

339
00:34:46,099 --> 00:34:47,391
Turn!

340
00:35:06,557 --> 00:35:10,391
- Turn it around!
- Turn it around!

341
00:35:13,224 --> 00:35:15,224
Get back! Back!

342
00:35:19,808 --> 00:35:21,182
Fire!

343
00:35:41,432 --> 00:35:43,724
I can see her!

344
00:35:43,808 --> 00:35:44,974
Ready!

345
00:35:45,057 --> 00:35:46,391
Move!

346
00:35:48,599 --> 00:35:49,724
Fire!

347
00:35:53,933 --> 00:35:55,891
- Reload!
- Reload!

348
00:35:55,974 --> 00:35:57,641
Faster!

349
00:35:57,724 --> 00:35:58,933
Hurry!

350
00:36:08,224 --> 00:36:09,265
Fire!

351
00:39:30,683 --> 00:39:32,349
Go!

352
00:39:35,599 --> 00:39:37,516
Run!

353
00:39:49,891 --> 00:39:50,974
Your Grace.

354
00:39:51,974 --> 00:39:54,307
All we need is one good shot.

355
00:39:54,391 --> 00:39:58,182
The scorpions have
all been destroyed, Your Grace.

356
00:40:00,015 --> 00:40:02,808
The Iron Fleet hold Blackwater Bay.

357
00:40:04,224 --> 00:40:06,099
Euron killed one of her dragons.

358
00:40:06,182 --> 00:40:07,599
He can kill another.

359
00:40:07,683 --> 00:40:10,849
Your Grace,
the Iron Fleet is burning.

360
00:40:10,933 --> 00:40:13,099
The gates have been breached.

361
00:40:13,182 --> 00:40:14,641
The Golden Company...

362
00:40:14,724 --> 00:40:18,182
Our men will fight harder
than sellswords ever could.

363
00:40:19,849 --> 00:40:21,933
They will defend their queen
to the last man.

364
00:40:22,015 --> 00:40:25,224
- Yes, Your Grace.
- The Red Keep has never fallen.

365
00:40:25,307 --> 00:40:27,265
It won't fall today.

366
00:40:56,182 --> 00:40:57,766
No, no, no!

367
00:41:00,974 --> 00:41:02,933
Move, move!

368
00:42:04,557 --> 00:42:05,974
Run.

369
00:42:06,057 --> 00:42:07,557
Run!

370
00:42:07,641 --> 00:42:08,808
Come on!

371
00:43:00,432 --> 00:43:03,891
- Ring the bells!
- Ring the bells!

372
00:43:04,766 --> 00:43:06,099
The bells!

373
00:43:06,182 --> 00:43:09,265
- Ring the bells!
- Ring the bells!

374
00:43:09,349 --> 00:43:11,474
Ring the bloody bells!

375
00:43:11,557 --> 00:43:14,224
- Ring the bells!
- Ring the bells!

376
00:43:15,641 --> 00:43:17,516
Tell the queen
to ring the bells.

377
00:43:17,599 --> 00:43:20,015
Ring the bells! Ring the bell!

378
00:43:28,224 --> 00:43:29,557
This way.

379
00:43:34,808 --> 00:43:36,265
Ring the bell!

380
00:43:37,265 --> 00:43:38,974
Ring the bell!

381
00:43:39,057 --> 00:43:40,391
Ring them!

382
00:43:43,683 --> 00:43:45,766
Ring the bells!

383
00:43:49,140 --> 00:43:51,641
Ring the bells!

384
00:43:51,724 --> 00:43:55,182
- Ring the bells!
- Help us!

385
00:43:55,265 --> 00:43:56,557
In the name of the...

386
00:43:58,099 --> 00:44:00,057
Queen! We're surrounded!

387
00:44:07,265 --> 00:44:09,766
- Ring the bells!
- Now! Come on!

388
00:44:16,891 --> 00:44:17,933
Hurry up!

389
00:44:27,766 --> 00:44:29,015
Ring the bells!

390
00:46:53,933 --> 00:46:55,099
No!

391
00:46:55,182 --> 00:46:56,307
Get back!

392
00:46:56,391 --> 00:46:57,432
Get back!

393
00:46:58,933 --> 00:47:00,974
Stay! Stay!

394
00:48:23,766 --> 00:48:25,516
No, no, no, no, no.

395
00:48:34,599 --> 00:48:36,724
Quickly. Quickly.

396
00:48:38,766 --> 00:48:40,224
Stop!

397
00:49:03,557 --> 00:49:06,391
- Get off me! Get off, bitch!
- No, get off! Help! Help!

398
00:49:07,432 --> 00:49:08,516
No!

399
00:49:38,516 --> 00:49:39,724
Go!

400
00:49:41,099 --> 00:49:42,224
Go!

401
00:50:19,891 --> 00:50:21,766
Find somewhere to hide.

402
00:50:46,265 --> 00:50:47,516
My God!

403
00:51:06,474 --> 00:51:08,349
The Kingslayer.

404
00:51:10,849 --> 00:51:13,766
We need to get the
queen out of King's Landing.

405
00:51:13,849 --> 00:51:15,474
Listen.

406
00:51:15,557 --> 00:51:19,015
That's the sound of a city dying.

407
00:51:20,974 --> 00:51:22,140
It's over.

408
00:51:22,224 --> 00:51:24,057
Well, maybe for you.

409
00:51:25,349 --> 00:51:28,391
If you kill another king
before you die...

410
00:51:29,599 --> 00:51:31,349
they'll sing about you forever.

411
00:51:32,391 --> 00:51:34,057
You're no king.

412
00:51:34,140 --> 00:51:35,557
Oh, but I am.

413
00:51:37,349 --> 00:51:39,057
And I fucked the queen.

414
00:51:42,724 --> 00:51:43,766
If I win...

415
00:51:45,015 --> 00:51:46,474
I'll bring your head to Cersei

416
00:51:46,557 --> 00:51:49,641
so you can kiss her one last time.

417
00:53:33,307 --> 00:53:35,474
Your Grace, it isn't safe here
any longer.

418
00:53:35,557 --> 00:53:37,599
The Red Keep is the safest place
in the city.

419
00:53:37,683 --> 00:53:41,057
The Unsullied have
breached the gates of the Red Keep.

420
00:53:49,599 --> 00:53:52,766
Maegor's Holdfast
would be a better place

421
00:53:52,849 --> 00:53:54,224
to wait out the storm.

422
00:54:31,599 --> 00:54:33,557
You fought well...

423
00:54:34,557 --> 00:54:35,933
for a cripple.

424
00:55:20,265 --> 00:55:21,599
Another king for you.

425
00:55:36,849 --> 00:55:37,849
But I got you!

426
00:55:41,099 --> 00:55:42,140
I got you!

427
00:55:48,641 --> 00:55:51,474
I'm the man who killed Jaime Lannister.

428
00:56:15,933 --> 00:56:17,015
Go home, girl.

429
00:56:18,140 --> 00:56:19,724
The fire will get her,

430
00:56:19,808 --> 00:56:21,557
or one of the Dothraki.

431
00:56:22,808 --> 00:56:25,516
Or maybe that dragon will eat her.

432
00:56:25,599 --> 00:56:27,599
It doesn't matter. She's dead.

433
00:56:27,683 --> 00:56:30,474
And you'll be dead too
if you don't get out of here.

434
00:56:31,516 --> 00:56:32,891
I'm going to kill her.

435
00:56:34,432 --> 00:56:37,265
You think you wanted revenge
a long time?

436
00:56:37,349 --> 00:56:38,974
I've been after it all my life.

437
00:56:39,057 --> 00:56:41,224
It's all I care about.

438
00:56:41,307 --> 00:56:43,683
And look at me. Look at me!

439
00:56:45,683 --> 00:56:48,015
You wanna be like me?

440
00:56:54,849 --> 00:56:59,057
You come with me, you die here.

441
00:57:12,432 --> 00:57:13,849
Sandor.

442
00:57:20,724 --> 00:57:22,391
Thank you.

443
00:58:24,683 --> 00:58:26,432
Your Grace.

444
00:58:43,140 --> 00:58:45,474
Hello, big brother.

445
00:58:51,391 --> 00:58:53,974
Ser Gregor, stay by my side.

446
00:59:03,891 --> 00:59:05,265
Ser Gregor, I command you.

447
00:59:05,349 --> 00:59:06,891
Obey your queen, Ser Gregor.

448
01:00:04,224 --> 01:00:06,933
Yeah, that's you.

449
01:00:09,182 --> 01:00:11,057
That's what you've always been.

450
01:01:45,849 --> 01:01:47,140
You're hurt.

451
01:01:49,015 --> 01:01:50,349
It doesn't matter.

452
01:01:58,015 --> 01:01:59,849
You're bleeding.

453
01:03:42,391 --> 01:03:44,349
Alanna!

454
01:03:44,432 --> 01:03:46,849
Alanna! Have you seen my wife?

455
01:03:46,933 --> 01:03:48,724
Have you seen my wife?

456
01:03:48,808 --> 01:03:50,432
- Let go.
- Have you seen my wife?

457
01:04:06,349 --> 01:04:07,808
Out of the way!

458
01:04:35,391 --> 01:04:36,683
No!

459
01:05:07,557 --> 01:05:09,015
Oh.

460
01:05:38,307 --> 01:05:39,808
Take my hand.

461
01:05:39,891 --> 01:05:41,808
Take my hand.

462
01:05:41,891 --> 01:05:43,349
Get up. Get up.

463
01:05:52,724 --> 01:05:55,140
- No. No.
- Wait! Wait!

464
01:05:55,224 --> 01:05:56,224
No!

465
01:06:40,766 --> 01:06:42,307
Fucking die!

466
01:08:15,891 --> 01:08:18,265
We need to fall back!

467
01:08:18,349 --> 01:08:21,224
Fall back behind the wall!

468
01:08:22,391 --> 01:08:23,974
- Fall back!
- Fall back!

469
01:08:24,057 --> 01:08:25,557
Get out of the city!

470
01:08:27,057 --> 01:08:28,349
Fall back!

471
01:08:32,057 --> 01:08:33,432
Fall back!

472
01:08:33,516 --> 01:08:35,182
- Fall back!
- Fall back!

473
01:10:31,349 --> 01:10:33,724
You can't stay here.

474
01:10:33,808 --> 01:10:35,432
You have to keep moving.

475
01:10:35,516 --> 01:10:37,099
We can't go out there.

476
01:10:37,182 --> 01:10:38,182
You have to.

477
01:10:39,391 --> 01:10:41,224
Everyone out there is dead.

478
01:10:42,516 --> 01:10:44,683
If you stay here, you'll die.

479
01:10:46,307 --> 01:10:47,307
Follow me.

480
01:10:49,182 --> 01:10:50,224
Follow me!

481
01:10:53,766 --> 01:10:55,516
Come on, quickly. Come to me.

482
01:10:55,599 --> 01:10:57,391
Lean him up there.

483
01:10:57,474 --> 01:10:59,391
Come on, quickly. All of you!

484
01:11:03,391 --> 01:11:04,391
Run!

485
01:11:10,224 --> 01:11:11,224
Run!

486
01:11:16,057 --> 01:11:18,057
Mama!

487
01:11:18,140 --> 01:11:19,432
Mama!

488
01:11:19,516 --> 01:11:21,474
Mama! No.

489
01:11:21,557 --> 01:11:23,432
Mama! Mama!

490
01:11:24,766 --> 01:11:25,766
Mama!

491
01:11:30,557 --> 01:11:32,349
Get up.

492
01:11:32,432 --> 01:11:33,849
Get up!

493
01:11:37,766 --> 01:11:38,974
We have to keep moving.

494
01:11:40,516 --> 01:11:42,015
Take her.

495
01:11:42,099 --> 01:11:43,974
Take her!

496
01:11:44,057 --> 01:11:46,557
- Take her.
- Come on!

497
01:11:46,641 --> 01:11:47,641
Come on!

498
01:12:08,265 --> 01:12:09,265
This way.

499
01:12:59,641 --> 01:13:02,140
I want our baby to live.

500
01:13:04,974 --> 01:13:07,349
I want our baby to live.

501
01:13:09,933 --> 01:13:11,683
I want our baby to live.

502
01:13:13,349 --> 01:13:15,891
Don't let me die, Jaime.
Please don't let me die.

503
01:13:15,974 --> 01:13:17,099
It's all right.

504
01:13:17,182 --> 01:13:19,015
- Please don't let me die.
- It's all right.

505
01:13:19,099 --> 01:13:20,099
I don't want to die.

506
01:13:22,849 --> 01:13:24,599
Just look... Look at me.

507
01:13:24,683 --> 01:13:26,724
- Look at me.
- Not like this.

508
01:13:26,808 --> 01:13:28,265
Not like this. Not like this.

509
01:13:28,349 --> 01:13:30,099
Look... Look... Look me in the eye.

510
01:13:30,182 --> 01:13:31,641
Don't look away. Don't look...

511
01:13:31,724 --> 01:13:33,974
Look at me! Just look at me.

512
01:13:38,516 --> 01:13:40,724
Nothing else matters.

513
01:13:43,265 --> 01:13:45,307
Nothing else matters.

514
01:13:45,391 --> 01:13:47,265
Only us.

515
01:18:00,933 --> 01:18:08,933
<font color="#ec14bd">Sync & corrections by honeybunny
www.NONTONKITA.com</font>