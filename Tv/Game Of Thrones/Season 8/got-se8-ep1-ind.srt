1
00:00:01,633 --> 00:00:03,882
Kita menderita
karena ulah kita sendiri.

2
00:00:04,549 --> 00:00:07,257
Kita kehilangan orang tercinta
karena ulah kita sendiri.

3
00:00:10,132 --> 00:00:11,924
Ini bukan soal nama baik suku,

4
00:00:12,299 --> 00:00:14,591
melainkan tentang hidup dan mati.

5
00:00:14,674 --> 00:00:16,299
Dan aku ingin tetap bersama yang hidup.

6
00:00:16,382 --> 00:00:17,965
♪

7
00:00:18,049 --> 00:00:19,674
Aku beri kau penawaran.

8
00:00:19,882 --> 00:00:21,382
Berlutut dan bergabung denganku.

9
00:00:21,674 --> 00:00:23,591
Atau menolak dan mati.

10
00:00:27,882 --> 00:00:30,132
Dia datang untuk mengambil alih Seven Kingdoms.

11
00:00:30,341 --> 00:00:32,299
Wilayah utara termasuk dari seven kingdoms.

12
00:00:34,382 --> 00:00:37,132
Pasukan Night King bertambah setiap hari.

13
00:00:41,090 --> 00:00:43,341
Kita butuh sekutu.
Sekutu yang kuat.

14
00:00:49,466 --> 00:00:50,840
Monter itu nyata.

15
00:00:52,132 --> 00:00:54,965
Naga, White Walkers,
kebringasan Dothraki...

16
00:00:56,216 --> 00:00:57,508
Biarkan mereka saling bunuh.

17
00:00:57,882 --> 00:00:59,716
Kita hadapi sisanya.

18
00:00:59,799 --> 00:01:02,007
♪

19
00:01:03,132 --> 00:01:04,924
Kita akan membinasakan Night King

20
00:01:05,007 --> 00:01:06,965
dan pasukannya,
dan kita lakukan bersama.

21
00:01:08,508 --> 00:01:11,174
Dia bersumpah untuk mengabdi pada Daenerys Targaryen.

22
00:01:11,466 --> 00:01:12,466
Dia sudah berlutut.

23
00:01:12,924 --> 00:01:14,633
Dia harus tahu yang sebenarnya.

24
00:01:17,341 --> 00:01:19,049
Jon bukanlah anak ayahku.

25
00:01:19,633 --> 00:01:21,508
Dia keturunan Rhaegar Targaryen,

26
00:01:21,716 --> 00:01:23,341
dan bibiku, Lyanna Stark.

27
00:01:24,508 --> 00:01:26,216
Dia pewaris dari Iron Throne.

28
00:01:26,799 --> 00:01:28,174
Kita harus beritahu dia.

29
00:01:33,299 --> 00:01:35,799
Musibah akan segera datang...

30
00:01:38,840 --> 00:01:39,882
Dan musibah itu nyata.

31
00:01:52,382 --> 00:01:54,216
♪

32
00:06:21,840 --> 00:06:23,758
Kau beruntung.

33
00:06:25,299 --> 00:06:27,132
Setidaknya kemaluannya tak membeku.

34
00:06:27,216 --> 00:06:29,341
Kau sangat tersinggung
dengan guyonan orang kerdil,

35
00:06:29,424 --> 00:06:31,965
tapi senang berguyon soal pria yang dikebiri.
Kenapa?

36
00:06:33,216 --> 00:06:36,132
Karena aku memiliki kemaluan
dan kau tidak.

37
00:07:03,674 --> 00:07:05,174
Sudah kuperingati.

38
00:07:05,257 --> 00:07:08,007
Orang utara tak mudah percaya orang asing.

39
00:08:29,633 --> 00:08:31,174
Lihat dirimu.

40
00:08:32,341 --> 00:08:33,591
Kau sudah dewasa.

41
00:08:34,591 --> 00:08:35,633
Hampir.

42
00:08:55,299 --> 00:08:58,174
Di mana Arya?/ Berkeliaran.

43
00:09:04,799 --> 00:09:07,257
Ratu Daenerys dari Keluarga Targaryen.

44
00:09:08,549 --> 00:09:12,382
Saudariku, Sansa Stark,
bangsawan Winterfell.

45
00:09:12,466 --> 00:09:15,424
Terima kasih undangannya, Ny. Stark.

46
00:09:15,508 --> 00:09:16,882
Wilayah Utara memang indah

47
00:09:16,965 --> 00:09:20,299
seperti yang dikatakan saudaramu,
kau juga.

48
00:09:23,799 --> 00:09:26,174
Winterfell milikmu, Yang Mulia.

49
00:09:27,633 --> 00:09:29,466
Kita tak punya waktu untuk berbasa-basi.

50
00:09:29,549 --> 00:09:33,341
Night King menguasai nagamu.
Dia memiliki satu sekarang.

51
00:09:34,341 --> 00:09:35,799
Tembok sudah runtuh.

52
00:09:35,882 --> 00:09:38,049
Para zombi mulai menuju ke utara.

53
00:09:43,341 --> 00:09:45,257
Setelah kita mendengar
tentang runtuhnya Tembok.

54
00:09:45,341 --> 00:09:48,466
Aku panggil semua suku untuk menuju Winterfell.

55
00:09:49,716 --> 00:09:50,716
Tn. Umber...

56
00:09:52,049 --> 00:09:55,007
kapan pasukanmu datang?

57
00:09:59,216 --> 00:10:03,174
Kita butuh lagi kuda dan kereta,
jika memungkinkan, Nyonya.

58
00:10:05,508 --> 00:10:07,132
Tuan.

59
00:10:09,466 --> 00:10:12,965
Ratuku. Maaf.

60
00:10:13,049 --> 00:10:15,007
Kau akan mendapatkannya.

61
00:10:15,090 --> 00:10:18,299
Kembalilah ke Last Hearth
dan bawa pasukannya ke sini.

62
00:10:21,216 --> 00:10:24,007
Kita kirim gagak ke pasukan Night's Watch juga.

63
00:10:24,090 --> 00:10:26,549
Tidak ada gunanya mengawasi benteng lagi.

64
00:10:26,633 --> 00:10:27,716
Kita berjaga di sini.

65
00:10:27,799 --> 00:10:28,882
Baik, Yang Mulia.

66
00:10:29,882 --> 00:10:31,424
"Yang Mulia."

67
00:10:37,049 --> 00:10:38,924
Tapi itu bukan statusmu, kan?

68
00:10:39,882 --> 00:10:41,382
Kau meninggalkan Winterfell

69
00:10:41,466 --> 00:10:42,882
dan kembali sebagai...

70
00:10:45,382 --> 00:10:47,341
Aku tak tahu apa statusmu sekarang.

71
00:10:47,424 --> 00:10:49,965
Seorang Tuan?

72
00:10:50,049 --> 00:10:52,341
Bukan sama sekali?

73
00:10:52,424 --> 00:10:54,633
Tak penting./ Tak penting?

74
00:10:55,799 --> 00:10:58,007
Kita melantikmu sebagai Raja Wilayah Utara.

75
00:10:59,591 --> 00:11:01,090
Raja Wilayah Utara.

76
00:11:06,216 --> 00:11:07,965
Benar, Nyonya.

77
00:11:09,508 --> 00:11:11,341
Sebuah kehormatan.

78
00:11:11,424 --> 00:11:14,132
Aku akan terus bersyukur.

79
00:11:16,257 --> 00:11:17,508
Saat aku meninggalkan Winterfell,

80
00:11:17,591 --> 00:11:20,257
Sudah ku kukatakan
kita butuh sekutu atau kita mati.

81
00:11:21,424 --> 00:11:25,299
Aku bawa sekutu untuk berjuang bersama.

82
00:11:26,591 --> 00:11:28,257
Sudah kutentukan pilihan.

83
00:11:28,341 --> 00:11:32,382
mempertahankan jabatanku
atau melindungi Wilayah Utara.

84
00:11:32,466 --> 00:11:34,341
Aku pilih Wilayah Utara.

85
00:11:46,716 --> 00:11:49,049
Jika ada yang selamat kelak,

86
00:11:49,132 --> 00:11:50,799
itulah jasa dari Jon Snow.

87
00:11:52,257 --> 00:11:54,716
Dia mempertaruhkan nyawanya
demi membuktikan ancaman itu nyata.

88
00:11:54,799 --> 00:11:56,591
Terimakasih atas keberaniannya,

89
00:11:56,674 --> 00:11:59,882
kita mempunyai pasukan terhebat
yang pernah ada.

90
00:11:59,965 --> 00:12:03,174
Kita mempunyai 2 naga dewasa.

91
00:12:05,341 --> 00:12:08,882
Dan kelak, pasukan dari Lannister
akan bergabung.

92
00:12:13,508 --> 00:12:17,674
Aku sadar, dulu kita berperang di masa lalu.

93
00:12:20,007 --> 00:12:21,716
Tapi kita harus berjuang bersama sekarang...

94
00:12:22,840 --> 00:12:23,840
atau mati.

95
00:12:25,882 --> 00:12:30,382
Bagaimana kita memberi makan
pasukan terhebat yang pernah ada itu?

96
00:12:30,466 --> 00:12:33,090
Aku pastikan makanan ada
sampai musim dingin,

97
00:12:33,174 --> 00:12:34,924
tapi tidak untuk Dothraki,

98
00:12:35,007 --> 00:12:38,716
Pasukan Unsullied
and dua ekor naga.

99
00:12:40,341 --> 00:12:42,341
Lagi pula apa makananan naga?

100
00:12:44,508 --> 00:12:45,840
Apapun yang mereka mau.

101
00:13:04,299 --> 00:13:08,132
Hati-hati.
Kita butuh itu.

102
00:13:08,216 --> 00:13:09,508
Maaf.

103
00:13:13,716 --> 00:13:15,341
Ini kereta terakhir?

104
00:13:15,424 --> 00:13:16,840
Benar.

105
00:13:16,924 --> 00:13:19,549
Baik.
Pindah ke pandai besi.

106
00:13:27,549 --> 00:13:28,549
Tuan.

107
00:13:30,716 --> 00:13:31,716
Nyonya.

108
00:13:38,341 --> 00:13:39,382
Nyonya.

109
00:13:51,382 --> 00:13:52,799
Bangsawan Winterfell.

110
00:13:54,633 --> 00:13:56,591
Bagus juga.

111
00:13:56,674 --> 00:13:58,549
Begitu juga Tangan Kanan Ratu.

112
00:13:59,840 --> 00:14:01,882
Tergantung ratunya.

113
00:14:04,591 --> 00:14:07,299
Terkahir kita bicara saat pernikahan Joffrey.

114
00:14:08,591 --> 00:14:11,382
Sebuah petaka./ Masa lalu.

115
00:14:17,090 --> 00:14:19,382
Maaf telah meninggalkanmu.

116
00:14:19,466 --> 00:14:21,007
Memang susah untuk dijelaskan,

117
00:14:21,090 --> 00:14:23,840
kenapa istriku kabur
setelah pembunuhan raja

118
00:14:26,424 --> 00:14:27,633
Kita berdua selamat.

119
00:14:30,591 --> 00:14:32,299
Banyak yang meremehkanmu.

120
00:14:33,549 --> 00:14:35,132
Kebanyakan dari mereka tewas sekarang.

121
00:14:43,090 --> 00:14:46,758
Aku yakin kau tak kaget
mendengar Pasukan Lannister menuju utara.

122
00:14:48,924 --> 00:14:52,049
Wajar jika kau khawatir dengan saudariku.

123
00:14:52,132 --> 00:14:53,591
Tak ada yang mengkhawatirkannya selain aku.

124
00:14:53,674 --> 00:14:56,591
Tapi aku janju, kau aman/
Cersei mengatakan padamu

125
00:14:56,674 --> 00:14:59,424
pasukannya datang ke Utara untuk membantumu?

126
00:14:59,508 --> 00:15:00,508
Benar.

127
00:15:02,132 --> 00:15:03,549
Dan kau percaya?

128
00:15:04,965 --> 00:15:06,674
Dia juga ingin hidup.

129
00:15:06,758 --> 00:15:09,549
Aku yakin dia ingin selamat.

130
00:15:12,007 --> 00:15:14,508
Aku dulu menganggap kau pria
paling pintar sedunia.

131
00:15:46,508 --> 00:15:48,174
Kau dulu lebih tinggi.

132
00:15:50,965 --> 00:15:52,508
Kenapa aku tak dengar kau di sini?

133
00:15:52,591 --> 00:15:55,424
Bagaimana kau hidup
setelah tertusuk jantungmu?

134
00:15:55,508 --> 00:15:57,007
Aku tidak selamat.

135
00:16:15,341 --> 00:16:16,591
Kau masih menyimpannya.

136
00:16:19,799 --> 00:16:21,007
Needle.

137
00:16:23,049 --> 00:16:24,924
Pernah kau gunakan?

138
00:16:25,840 --> 00:16:27,591
Sekali dua kali.

139
00:16:42,924 --> 00:16:44,758
Baja Valyrian.

140
00:16:46,341 --> 00:16:47,341
Kau mau?

141
00:16:47,382 --> 00:16:50,549
Terlalu berat untukku.

142
00:16:58,716 --> 00:17:00,591
Kau dari mana saja?

143
00:17:00,674 --> 00:17:03,382
Aku butuh bantuanmu dan Sansa.

144
00:17:05,424 --> 00:17:07,633
Dia tak suka ratu pilihanmu ya?

145
00:17:07,716 --> 00:17:10,466
Sansa pikir dia lebih pintar dari semua orang.

146
00:17:11,799 --> 00:17:13,840
Dia orang terpintar yang kutemui.

147
00:17:15,216 --> 00:17:16,508
Sekarang kau melindunginya?

148
00:17:16,591 --> 00:17:17,965
Kau?

149
00:17:19,299 --> 00:17:21,090
Aku melindunginya keluarga kita.

150
00:17:22,466 --> 00:17:23,633
Dia juga.

151
00:17:23,716 --> 00:17:25,132
Ya.

152
00:17:26,591 --> 00:17:28,174
Aku keluarganya juga.

153
00:17:34,216 --> 00:17:35,466
Jangan lupa.

154
00:17:53,299 --> 00:17:56,674
Yang Mulia, aku membawa berita buruk.

155
00:17:57,840 --> 00:17:59,840
Para zombi sudah menembuh Tembok.

156
00:18:03,466 --> 00:18:04,716
Bagus.

157
00:18:38,924 --> 00:18:41,299
Kenapa kau tak selesaikan dan bunuh aku?

158
00:18:41,382 --> 00:18:44,341
Kita keluarga.

159
00:18:45,549 --> 00:18:48,299
Keturunan Greyjoys terakhir.

160
00:18:51,216 --> 00:18:53,466
Keturunan yang pemberani.

161
00:18:58,424 --> 00:18:59,758
Jika aku membunuhmu...

162
00:19:01,216 --> 00:19:02,716
aku ngobrol dengan siapa?

163
00:19:03,633 --> 00:19:05,007
Hmm?

164
00:19:07,549 --> 00:19:09,882
Para pasukanku pendiam.

165
00:19:13,216 --> 00:19:14,924
Aku bisa kesepian di laut.

166
00:19:17,674 --> 00:19:19,591
Kita di King's Landing?

167
00:19:19,674 --> 00:19:20,758
Mm.

168
00:19:25,299 --> 00:19:27,424
Kau memilih kelompok yang kalah.

169
00:19:31,090 --> 00:19:33,758
Tinggal ku belokkan Iron Fleet
ke tempat lain.

170
00:19:40,840 --> 00:19:43,758
Pertama, aku akan bercumbu dengan ratu.

171
00:19:57,299 --> 00:19:59,049
dua puluh ribu orang?Twenty-thousand men, is it?

172
00:19:59,132 --> 00:20:02,174
Benar, Yang Mulia.
Ada yang tewas di perjalanan.

173
00:20:04,466 --> 00:20:05,965
Karena bermain curang.

174
00:20:07,924 --> 00:20:09,882
Atau aku yang curang.

175
00:20:09,965 --> 00:20:11,090
Pasti ada yang curang.

176
00:20:13,174 --> 00:20:15,716
Mereka bukan ksatria kuat.
Tidak masalah.

177
00:20:15,799 --> 00:20:17,716
Kuda?/ Dua ribu.

178
00:20:17,799 --> 00:20:19,049
Dan gajah?

179
00:20:19,132 --> 00:20:21,508
Tak ada gajha, Yang Mulia.

180
00:20:23,132 --> 00:20:24,299
Sayang sekali.

181
00:20:25,674 --> 00:20:27,840
Setahuku Golden Company punya hajah.

182
00:20:27,924 --> 00:20:30,299
Mereka hewan kuat, Yang Mulia.

183
00:20:30,382 --> 00:20:33,882
tapi tak cocok di perjalanan laut.

184
00:20:33,965 --> 00:20:37,758
Kau sangat disambut di King's Landing,
Captain Strickland.

185
00:20:37,840 --> 00:20:41,049
Kita siap berjuang untukkmu, Yang Mulia.

186
00:20:52,007 --> 00:20:53,674
Apa aku juga disambut di sini?

187
00:20:55,174 --> 00:20:59,758
Kau adalah teman sejati dari kerajaan
dan tamu terhormat.

188
00:20:59,840 --> 00:21:01,090
Bagus.

189
00:21:01,174 --> 00:21:04,216
Sebagai teman sejati
dan tamu terhormat...

190
00:21:13,049 --> 00:21:16,216
Aku berharap kita bisa berbicara berdua.

191
00:21:17,424 --> 00:21:19,633
Setelah perang.
Itu perjanjian kita.

192
00:21:19,716 --> 00:21:23,549
Perang bisa bertahun-tahun.

193
00:21:24,716 --> 00:21:27,216
Kau ingin pelacur, sewa sana.

194
00:21:29,007 --> 00:21:30,382
Kau ingin ratu...

195
00:21:31,674 --> 00:21:33,382
berusahalah.

196
00:21:36,257 --> 00:21:37,257
Bagaimana?

197
00:21:38,341 --> 00:21:39,674
Aku beri dia keadilan,

198
00:21:39,758 --> 00:21:42,466
pasukan, dan Iron Fleet,

199
00:21:42,549 --> 00:21:45,591
namun dia tak memberiku
sedikit perhatian.

200
00:21:46,549 --> 00:21:48,716
Hatiku mulai retak.

201
00:21:51,007 --> 00:21:52,257
Kau banyak bicara.

202
00:21:53,965 --> 00:21:58,382
Aku eksekusi pria seperti itu./
Mereka memang cocok.

203
00:22:37,174 --> 00:22:40,090
Aku dengar naga membakar
ribuan pasukan Lannister.

204
00:22:40,174 --> 00:22:42,090
Membakar pria kesukaanku.

205
00:22:42,174 --> 00:22:43,840
Archie, kan?/ Dan William.

206
00:22:43,924 --> 00:22:46,924
William ganteng tinggi?/
Ya, William ganteng tinggi.

207
00:22:47,007 --> 00:22:49,674
Mereka bilang jasadnya hialng.

208
00:22:49,758 --> 00:22:53,216
Hanya aku pria yang menembak naga.

209
00:22:53,299 --> 00:22:55,049
Benarkah?/
Hampir membunuhnya.

210
00:22:55,132 --> 00:22:56,132
Berani juga.

211
00:23:06,424 --> 00:23:08,965
Si Eddie.../ Rambut merah?

212
00:23:09,049 --> 00:23:12,216
Benar. Dia selamat tapi
wajahnya hancur.

213
00:23:12,299 --> 00:23:13,549
Tidak punya kelopak mata.

214
00:23:13,633 --> 00:23:15,257
Bagaimana dia bisa tidur?

215
00:23:15,341 --> 00:23:17,840
Bisa kita berhenti berbicara naga?

216
00:23:17,924 --> 00:23:19,799
Tn. Bronn dari Blackwater.

217
00:23:22,007 --> 00:23:23,090
Kau brecanda?

218
00:23:23,174 --> 00:23:24,591
Maaf mengganggu.

219
00:23:24,674 --> 00:23:27,007
Ratu memerintahku untuk cepat.

220
00:23:29,424 --> 00:23:30,633
Maaf, sayang.

221
00:23:32,674 --> 00:23:34,216
Lain kali saja.

222
00:23:41,924 --> 00:23:45,674
Jika kau kesepian,
aku mau kok pria tua.

223
00:23:48,716 --> 00:23:51,965
Wanita malang.
Dia akan kena cacar dalam setahuan.

224
00:23:53,591 --> 00:23:55,591
Yang mana?/
Saudara ratu,

225
00:23:55,674 --> 00:23:58,299
berjanji padamu dan mengingkarinya.

226
00:23:58,382 --> 00:24:00,674
Ratu ingin memperbaiki kesalahannya.

227
00:24:02,382 --> 00:24:04,799
Dia dulu memberiku kastil dan istri,

228
00:24:04,882 --> 00:24:06,549
lalu mencabutnya begitu saja.

229
00:24:06,633 --> 00:24:08,924
Itu pemberian Tn. Jaime,
bukan Ratu.

230
00:24:09,007 --> 00:24:12,758
Ketika Ratu Cersei ingin sesuatu,
dia membayar emas di awal.

231
00:24:12,840 --> 00:24:14,591
Beberapa peti malahan.

232
00:24:14,674 --> 00:24:17,882
Menunghu mu di luar.

233
00:24:22,466 --> 00:24:27,299
Dia ingin membunuh seseoarang,
tapi dia tak mau mengirim pasukan,

234
00:24:27,382 --> 00:24:29,466
Jika Si Ratu Naga yang dia incar...

235
00:24:29,549 --> 00:24:31,633
Dia punya rencana lain
untuk gadis Targaryen itu.

236
00:24:31,716 --> 00:24:33,799
Semoga beruntung.

237
00:24:33,882 --> 00:24:38,132
Saudara ratu tak akan selamat
di perjalanan menuju Utara.

238
00:24:38,216 --> 00:24:40,549
Tapi jika dia selamat...

239
00:24:47,090 --> 00:24:50,758
Dia ingin menegakkan keadilan.

240
00:24:52,007 --> 00:24:53,840
Keluarga sialan.

241
00:24:53,924 --> 00:24:55,674
Ketika Citadel memecatku,

242
00:24:55,758 --> 00:24:57,591
aku kira aku akan mati miskin.

243
00:24:57,674 --> 00:25:01,674
Tapi karena jasaku,
dia menjadikanku kaki tangannya.

244
00:25:01,758 --> 00:25:03,633
Apa yang dia lakukan untuk orang

245
00:25:03,716 --> 00:25:06,090
yang berjasa membunuh
saudaranya yang berkhianat?

246
00:25:22,216 --> 00:25:24,591
Aku ingin gajah padahal.

247
00:25:30,674 --> 00:25:31,674
Jadi...

248
00:25:33,466 --> 00:25:37,466
Hebat aku atau si raja gendut itu?

249
00:25:38,466 --> 00:25:40,299
Kau menghina mantan suamiku?

250
00:25:40,382 --> 00:25:41,633
Kau tersinggung?

251
00:25:46,216 --> 00:25:48,882
Robert sewa banyak pelacur setiap malam.

252
00:25:48,965 --> 00:25:51,965
Tapi dia masih tak tahu
cara memuaskan wanita.

253
00:25:52,924 --> 00:25:54,007
Dan si Kingslayer?

254
00:25:59,591 --> 00:26:01,633
Kau senang digantung?

255
00:26:03,299 --> 00:26:04,466
Hidup memang membosankan.

256
00:26:04,549 --> 00:26:07,674
Kau tak bosan.
Aku sudah melayanimu.

257
00:26:19,674 --> 00:26:21,633
Apa Ratu terpuaskan?

258
00:26:23,090 --> 00:26:26,466
Kau orang yang paling arogan yang kutemui.

259
00:26:28,633 --> 00:26:30,007
Aku suka itu.

260
00:26:31,882 --> 00:26:34,341
Tapi, aku ingin sendiri sekarang.

261
00:26:40,799 --> 00:26:43,633
Aku akan memberi
pangeran di perutmu.

262
00:28:17,924 --> 00:28:20,549
Euron tak bisa mempertahankan Iron Islands,

263
00:28:20,633 --> 00:28:24,132
karena dia di King's Landing
dengan seluruh pasukan dan kapalnya.

264
00:28:25,257 --> 00:28:26,882
Kita bisa merebut rumah kita kembali.

265
00:28:26,965 --> 00:28:28,466
Daenerys menuju utara.

266
00:28:28,549 --> 00:28:32,090
Daenerys butuh tempat jika dia
kalah perang di Utara.

267
00:28:32,174 --> 00:28:34,924
Tempat yang tak terjamah zombi.

268
00:28:40,216 --> 00:28:43,466
Kau ratuku.
Aku menuruti perintahmu.

269
00:28:48,382 --> 00:28:50,382
Kau ingin ke Winterfell.

270
00:28:50,466 --> 00:28:51,965
Berjuang bersama keluarga Starks.

271
00:28:55,840 --> 00:28:56,840
Pergilah.

272
00:29:01,633 --> 00:29:03,716
Yang sudah mati takkan bisa mati.

273
00:29:07,216 --> 00:29:09,174
Yang sudah mati takkan bisa mati.

274
00:29:13,882 --> 00:29:16,132
Tapi bunuh anak haram itu.

275
00:29:43,924 --> 00:29:45,591
Selanat datang, Nyonya.

276
00:29:46,882 --> 00:29:48,299
Silahkan ikuti aku...

277
00:29:48,382 --> 00:29:49,591
KPasukan arstarks.

278
00:29:49,674 --> 00:29:51,840
Lambang gergaji.

279
00:29:53,007 --> 00:29:54,382
Untuk potong bawang.

280
00:29:55,882 --> 00:29:57,466
Bisa jadi.

281
00:29:57,549 --> 00:29:59,674
Peristiwa lalu,
Keluarga Starks dan Karstarks

282
00:29:59,758 --> 00:30:02,132
saling bantai di medan perang.

283
00:30:02,216 --> 00:30:04,216
Jon Snow mendamaikan mereka.

284
00:30:04,299 --> 00:30:06,257
Dan ratu berterima kasih.

285
00:30:06,341 --> 00:30:09,591
Senang mendengarnya,
tapi bukan itu maksudku.

286
00:30:09,674 --> 00:30:12,716
Orang Utara setia pada Jon Snow,
bukan Ratu.

287
00:30:12,799 --> 00:30:13,882
Mereka belum mengenalnya.

288
00:30:13,965 --> 00:30:16,299
Pasukan liar tak mengenalnya.

289
00:30:16,382 --> 00:30:21,132
Aku di sini sudah lama,
dan mereka sangat keras kepala.

290
00:30:21,216 --> 00:30:23,840
Kau ingin mendapatkan kesetiaannya,
kau harus berusaha.

291
00:30:35,257 --> 00:30:37,758
Kau mau mengajukan penawaran?

292
00:30:38,924 --> 00:30:41,549
Aku tak menawarkan apapun.

293
00:30:41,633 --> 00:30:45,007
Aku hanya ingin selamat dari Night King,

294
00:30:45,090 --> 00:30:49,132
bagaimana jika Seven Kingdoms,
dengan masa lalu yang kelam,

295
00:30:49,216 --> 00:30:52,758
diperintah seorang wanita dan
pria terhormat?

296
00:30:55,216 --> 00:30:57,049
Mereka pasangan yang cocok.

297
00:30:57,132 --> 00:30:58,882
Kau membesar-besarkan.

298
00:30:58,965 --> 00:31:02,341
Jon dan Daenerys tak ingin
nasehat orang tua yang kesepian.

299
00:31:02,424 --> 00:31:04,132
Aku tak setua itu.

300
00:31:05,424 --> 00:31:06,840
Tak setua dia.

301
00:31:08,633 --> 00:31:11,007
Ratu kita menghormati
orang yang lebih tua.

302
00:31:11,090 --> 00:31:12,466
Tentu saja.

303
00:31:12,549 --> 00:31:15,299
Hormatlah yang membuat
orang muda memiliki jarak,

304
00:31:15,382 --> 00:31:18,882
jadi kita tak perlu mengingatkan
kebenaran yang pahit.

305
00:31:18,965 --> 00:31:20,174
Apa itu?

306
00:31:21,299 --> 00:31:22,674
Tak ada yang abadi.

307
00:31:31,674 --> 00:31:34,132
Saudarimu tak menyukaiku.

308
00:31:38,674 --> 00:31:39,924
Dia belum mengenalmu.

309
00:31:41,840 --> 00:31:43,299
Supaya kau lega,

310
00:31:43,382 --> 00:31:45,192
dia juga tak menyukaiku waktu kecil dulu.

311
00:31:45,216 --> 00:31:47,341
Dia tak perlu jadi temanku...

312
00:31:48,924 --> 00:31:50,840
aku adalah ratunya.

313
00:31:54,341 --> 00:31:56,299
Jika dia tak menghormatiku...

314
00:32:07,780 --> 00:32:09,369
Berapa hari ini?

315
00:32:09,607 --> 00:32:12,061
Hanya 18 kambing dan 11 domba.

316
00:32:15,174 --> 00:32:16,424
Ada apa?

317
00:32:16,508 --> 00:32:19,007
Naga tak cukup makan.

318
00:32:36,382 --> 00:32:37,674
Ada apa dengan mereka?

319
00:32:37,758 --> 00:32:39,341
Mereka tak suka wilayah Utara.

320
00:32:55,591 --> 00:32:56,716
Naiklah.

321
00:33:02,716 --> 00:33:05,174
Aku tak bisa mengendarai kuda.

322
00:33:05,257 --> 00:33:07,758
Tak ada yang bisa.
Sampai mereka mengedarainya.

323
00:33:10,257 --> 00:33:11,758
Bagaimana jika dia tak menyukaiku?

324
00:33:11,840 --> 00:33:14,758
Maka sampai bertmu lagi, Jon Snow.

325
00:33:34,257 --> 00:33:35,882
Pegangan apa?

326
00:33:37,341 --> 00:33:38,716
Apapun.

327
00:35:35,174 --> 00:35:37,508
Kau mengubah caraku mengendarai kuda.

328
00:35:46,591 --> 00:35:48,716
Kita bisa sembunyi ribuan tahun...

329
00:35:50,882 --> 00:35:52,216
dan tak ada yang menemukan kita.

330
00:35:54,758 --> 00:35:56,090
Kita akan sangat tua.

331
00:36:04,257 --> 00:36:07,299
Tempat ini dingin untuk gadis selatan.

332
00:36:07,382 --> 00:36:09,090
Hangatkan ratumu.

333
00:36:23,799 --> 00:36:24,924
Jangan takut.

334
00:36:38,466 --> 00:36:39,633
Bagus.

335
00:36:48,591 --> 00:36:50,341
Gendry?/ Dia di sini.

336
00:36:56,466 --> 00:36:59,341
Susah mebuat senjata besar
menggunakan dragonglass.

337
00:37:01,508 --> 00:37:03,799
Katamu kau hebat.

338
00:37:03,882 --> 00:37:05,882
Maksudku bahannya sangat sudah untuk...

339
00:37:05,965 --> 00:37:09,007
Kau tahu siapa yang buat
sejata untuk wildlings?

340
00:37:09,090 --> 00:37:11,466
Orang pincang dan cacat.

341
00:37:11,549 --> 00:37:14,132
Kau yang mana?/ Jangan ganggu dia.

342
00:37:21,341 --> 00:37:22,924
Aku tahu kau di sini.

343
00:37:25,508 --> 00:37:26,965
Kau meninggalkanku sekarat.

344
00:37:28,007 --> 00:37:29,674
Aku merampokmuu juga.

345
00:37:41,132 --> 00:37:43,466
Kau gadis pembawa sial.

346
00:37:47,007 --> 00:37:49,216
Karena itu kau masih hidup.

347
00:37:56,924 --> 00:37:59,508
Kapak yang bagus.

348
00:37:59,591 --> 00:38:00,799
Kau makin ahli.

349
00:38:00,882 --> 00:38:03,341
Terimakasih. Kau juga.

350
00:38:04,549 --> 00:38:06,882
Kau tampak sehat.

351
00:38:08,382 --> 00:38:10,382
Kau juga.

352
00:38:14,049 --> 00:38:16,882
Winterfell ini nyaman jika tak terlalu dingin.

353
00:38:18,216 --> 00:38:19,924
Jangan menjauh dari tungku besi.

354
00:38:20,007 --> 00:38:23,633
Apa itu perintah, Nn. Lady Stark?/
Jangan panggil aku itu.

355
00:38:23,716 --> 00:38:25,882
Baiklah, Nona.

356
00:38:33,174 --> 00:38:34,382
Ini pesananku.

357
00:38:36,716 --> 00:38:37,840
Bisa kau buatkan?

358
00:38:37,924 --> 00:38:40,090
Kau gunakan untuk apa benda ini?

359
00:38:40,174 --> 00:38:41,424
Kau bisa bisa atau tidak?

360
00:38:41,508 --> 00:38:44,382
Kau sudah punya belati.
Apa itu?

361
00:38:51,007 --> 00:38:52,341
Besi Valyrian.

362
00:38:52,424 --> 00:38:54,799
Kau memang gadis yang suka bergaya.

363
00:38:56,758 --> 00:38:59,299
Hanya aku gadis yang kau kenal.

364
00:39:09,799 --> 00:39:10,799
Masuk.

365
00:39:18,341 --> 00:39:20,090
Tn Glover titip salam,

366
00:39:20,174 --> 00:39:23,508
tapi dia tetap di Deepwood Motte
bersama pasukannya.

367
00:39:26,090 --> 00:39:28,341
"Suku Glover tetap berjuang
bersama Suku Stark

368
00:39:28,424 --> 00:39:30,799
selama ribuan tahun ini.'

369
00:39:31,965 --> 00:39:33,257
Pasti dia menulis itu.

370
00:39:33,341 --> 00:39:36,257
"Aku akan berjuang bersama Jon Snow"
dia tulis.

371
00:39:37,799 --> 00:39:39,132
"Raja Wilayah Utara."

372
00:39:40,257 --> 00:39:42,007
Kita butuh sekutu.

373
00:39:42,090 --> 00:39:45,341
Kau tak mengatakan kau akan meninggalkan jabatanmu.

374
00:39:45,424 --> 00:39:47,549
Aku tak ingin jabatan.

375
00:39:47,633 --> 00:39:50,382
Aku hanya ingin melindungi Wilayah Utara.

376
00:39:50,466 --> 00:39:53,216
Aku bawa dua pasukan, dua naga.

377
00:39:53,299 --> 00:39:54,965
Dan Ratu Targaryen.

378
00:39:55,049 --> 00:39:58,716
Menurutmu kita bisa mengalahkan
pasukan zombi tanpa dia?

379
00:39:58,799 --> 00:40:01,758
Aku pernah melawan mereka, Sansa.
Dua kali.

380
00:40:01,840 --> 00:40:04,049
Kau khawatir tentang status,

381
00:40:04,132 --> 00:40:06,591
Itu tak penting.

382
00:40:06,674 --> 00:40:09,341
Tanpa dia,
kita takkan menang.

383
00:40:18,257 --> 00:40:20,840
Apa kau masih meyakiniku?

384
00:40:22,424 --> 00:40:23,716
Kau tahu aku yakin.

385
00:40:28,174 --> 00:40:29,799
Dia calon ratu yang bijaksana.

386
00:40:30,882 --> 00:40:31,965
Untuk kita semua.

387
00:40:33,633 --> 00:40:35,132
Dia tak seperti ayahnya.

388
00:40:41,674 --> 00:40:43,341
Tentu, dia lebih cantik.

389
00:40:48,840 --> 00:40:51,299
Kau berlutut untuk demi Wilayah Utara

390
00:40:51,382 --> 00:40:52,882
atau karena mencintainya?

391
00:41:21,591 --> 00:41:22,840
Jadi kau orangnya?

392
00:41:24,466 --> 00:41:26,508
Orang yang mana, Yang Mulia?

393
00:41:26,591 --> 00:41:29,840
Yang telah menyelamatkan Tn. Jorah
saat tak ada yang bisa.

394
00:41:29,924 --> 00:41:32,090
Mereka bisa tapi tak mau.

395
00:41:33,424 --> 00:41:35,341
Aku harus membalas budi

396
00:41:35,424 --> 00:41:37,174
di Citadel
saat aku bertahta.

397
00:41:37,257 --> 00:41:39,840
Kerja keras harus diberi hadiah.

398
00:41:39,924 --> 00:41:42,591
Kehormatanku untuk mengabdi, Yang Mulia.

399
00:41:42,674 --> 00:41:45,716
Harus ada sesuatu yang kuberikan padamu.

400
00:41:47,090 --> 00:41:49,840
Jika tak masalah,

401
00:41:49,924 --> 00:41:52,341
aku meminta pengampunan.

402
00:41:52,424 --> 00:41:53,674
Untuk kejahatan apa?

403
00:41:55,007 --> 00:41:57,758
Aku mengambil buku dari Citadel.

404
00:42:00,132 --> 00:42:02,174
Dan juga pedang.

405
00:42:02,257 --> 00:42:04,716
Dari Citadel?/
Dari keluargaku.

406
00:42:06,257 --> 00:42:08,132
Sudah lama tersimpan di Keluarga Tarly.

407
00:42:08,216 --> 00:42:11,299
Tak mungkin jadi milikku,

408
00:42:11,382 --> 00:42:14,341
ayahku punya rencana lain.

409
00:42:16,716 --> 00:42:18,090
Bukan Randyll Tarly?

410
00:42:20,049 --> 00:42:21,466
Kau kenal?

411
00:42:25,341 --> 00:42:29,216
Aku biarkan dia mempunyai tanah dan jabatan,
jika dia mau berlutut.

412
00:42:32,299 --> 00:42:33,424
Dia menolak.

413
00:42:46,633 --> 00:42:48,549
Paling tidak aku boleh pulang,

414
00:42:48,633 --> 00:42:50,299
sekarang saudaraku yang jadi penguasa.

415
00:42:54,341 --> 00:42:56,799
Saudaramu berperang bersama ayahmu.

416
00:43:13,840 --> 00:43:15,758
Terima kasih, Yang Mulia.

417
00:43:15,840 --> 00:43:17,257
Untuk informasinya.

418
00:43:19,174 --> 00:43:21,716
Boleh aku..?/
Tentu..

419
00:44:04,508 --> 00:44:06,382
Kenapa kau di luar?

420
00:44:07,591 --> 00:44:09,882
Menunggu teman lama.

421
00:44:11,216 --> 00:44:13,299
Saatnya memberi tahu Jon yang sebenarnya.

422
00:44:14,341 --> 00:44:16,341
Tidak.

423
00:44:16,424 --> 00:44:20,299
Kau saudaranya,
Kau yang sampaikan.

424
00:44:20,382 --> 00:44:22,508
Aku bukan saudaranya.

425
00:44:22,591 --> 00:44:24,882
Dia sangat mempercayaimu.

426
00:44:25,924 --> 00:44:27,924
Sekarang waktunya.

427
00:45:03,007 --> 00:45:04,466
Sam?

428
00:45:04,549 --> 00:45:08,341
Maaf, aku tak seharusnya di sini.

429
00:45:11,591 --> 00:45:13,965
Kau bersembunyi dariku?/
Tidak.

430
00:45:15,132 --> 00:45:17,216
Apa yang kau lakukan di Winterfell?

431
00:45:17,299 --> 00:45:19,965
Kau sudah baca seluruh buku di Citadel?

432
00:45:23,466 --> 00:45:25,174
Ada apa?

433
00:45:25,257 --> 00:45:26,716
Gilly? Dia sehat?

434
00:45:26,799 --> 00:45:28,633
Sehat./ Sam kecil?

435
00:45:30,758 --> 00:45:31,758
Kau tahu tidak?

436
00:45:33,132 --> 00:45:34,257
Tahu apa?

437
00:45:35,674 --> 00:45:36,674
Daenerys...

438
00:45:37,840 --> 00:45:41,007
she menghukum ayahku dan saudaraku.

439
00:45:41,090 --> 00:45:42,674
Mereka pernah jadi tahahannya.

440
00:45:46,633 --> 00:45:48,674
Dia tak memberi tahumu.

441
00:45:57,049 --> 00:45:58,341
Maaf.

442
00:46:01,840 --> 00:46:03,549
Kita harus mengakhiri perang.

443
00:46:05,299 --> 00:46:06,882
Apa kau tega melakukannya?

444
00:46:08,965 --> 00:46:10,924
Aku pernah menghukum orang
yang tak taat perintah.

445
00:46:11,007 --> 00:46:12,716
Kau juga membebaskannya.

446
00:46:12,799 --> 00:46:15,382
Ribuan wildlings saat menolak untuk berlutut.

447
00:46:15,466 --> 00:46:16,924
Aku bukan raja.

448
00:46:19,299 --> 00:46:20,341
Kau dulu raja.

449
00:46:22,049 --> 00:46:23,382
Sejak dulu kau raja.

450
00:46:25,341 --> 00:46:27,049
Aku sudah lepas jabatanku, Sam.

451
00:46:27,840 --> 00:46:28,840
Aku sudah berlutut.

452
00:46:28,924 --> 00:46:30,466
Aku bukan "Raja Utara" lagi.

453
00:46:30,549 --> 00:46:32,192
Aku tak membicarakan "Raja Utara".

454
00:46:32,216 --> 00:46:35,466
Aku membicarakan Raja dari Seven Kingdoms.

455
00:46:43,049 --> 00:46:44,758
Bran dan aku mencari tahu.

456
00:46:44,840 --> 00:46:47,674
Aku menemukan buku harian pendeta agung.
Bran mempunyai...

457
00:46:48,799 --> 00:46:50,132
apapun yang Bran punya.

458
00:46:50,216 --> 00:46:52,174
Apa maksudmu?

459
00:46:55,299 --> 00:46:56,341
Ibumu...

460
00:46:57,508 --> 00:46:58,840
Lyanna Stark.

461
00:47:00,424 --> 00:47:01,758
Dan ayahmu...

462
00:47:03,049 --> 00:47:05,007
ayah kandungmu

463
00:47:05,090 --> 00:47:06,758
Rhaegar Targaryen.

464
00:47:08,924 --> 00:47:11,549
Kau bukanlah anak haram.

465
00:47:11,633 --> 00:47:15,382
Kau Aegon Targaryen,
pewaris asli Iron Throne.

466
00:47:19,633 --> 00:47:22,424
Maaf, aku tahu berat rasanya.

467
00:47:31,633 --> 00:47:34,965
Ayahku orang yang paling terhormat.

468
00:47:38,508 --> 00:47:40,591
Artinya dia berbohong padaku selama ini?

469
00:47:40,674 --> 00:47:41,799
Bukan.

470
00:47:42,799 --> 00:47:46,299
Ayahmu...  Ned Stark.

471
00:47:46,382 --> 00:47:49,758
Dia berjanji pada ibumu
untuk melindungimu.

472
00:47:49,840 --> 00:47:52,633
Dia lakukan itu.
Robert membunuhmu jika dia tahu.

473
00:47:54,633 --> 00:47:56,132
Kau raja yang sesungguhnya.

474
00:47:57,299 --> 00:47:59,174
Aegon Targaryen,
Pewaris ke Enam.

475
00:47:59,257 --> 00:48:01,257
Pelindung Kerajaan. Semuanya.

476
00:48:15,799 --> 00:48:18,758
Daenerys adalah ratumu./
Itu salah.

477
00:48:20,674 --> 00:48:22,674
Ini pengkhianatan./
Ini kebenaran.

478
00:48:24,424 --> 00:48:27,007
Kau meninggalkan tahtamu
untuk keselamatan umat.

479
00:48:28,674 --> 00:48:30,341
Apakah dia mau berbuat sama?

480
00:50:38,549 --> 00:50:40,216
Mundur, dia bermata biru!

481
00:50:40,299 --> 00:50:41,924
Mataku biru sejak lahir!

482
00:50:51,965 --> 00:50:53,132
Kau menemukan orang lain?

483
00:51:26,508 --> 00:51:28,674
Anak pesuruh.

484
00:51:28,758 --> 00:51:29,882
Sebuah pesan.

485
00:51:31,007 --> 00:51:32,591
Dari Night King.

486
00:51:32,674 --> 00:51:36,341
Pasukannya ada di antara kita dan Winterfell.
Kita jalan kaki.

487
00:51:36,424 --> 00:51:38,257
Kita potong jalan lewat Castle Black.

488
00:51:38,341 --> 00:51:40,591
Kita bisa lebih cepat.

489
00:51:40,674 --> 00:51:44,591
Jika kudanya kuat.
kita sampai duluan.

490
00:51:44,674 --> 00:51:47,591
Semoga Night King tak sampai duluan.

491
00:52:39,257 --> 00:52:41,799
Terus! Lurus!

492
00:53:34,576 --> 00:53:39,051
Semarang, 15 April 2019

493
00:53:34,576 --> 00:53:41,295
Penerjemah: @indraama

494
00:53:41,295 --> 00:55:01,700
IDFL SUBSCREW
-BEST QUALITY-

495
00:55:01,716 --> 00:55:03,549
Ketika aku masih kecil,
saudaraku membacakan

496
00:55:03,633 --> 00:55:07,090
dongeng tentang orang yang membunuh ayah kita.

497
00:55:08,424 --> 00:55:10,882
Tentang semua hal yang akan kita lakukan.

498
00:55:13,549 --> 00:55:15,132
Seharusnya kau tak mempercayai Cercei.

499
00:55:15,216 --> 00:55:16,716
Kau juga.

500
00:55:18,007 --> 00:55:19,174
Kematian.

501
00:55:19,758 --> 00:55:23,132
punya banyak wajah,
aku menunggu untuk melihat ini.

502
00:55:24,508 --> 00:55:25,591
Berapa lama waktu kita?

503
00:55:26,549 --> 00:55:28,924
♪

504
00:55:30,424 --> 00:55:32,424
Sebelum matahari terbit.

